import sql from 'mysql2'

const config = {
  host: "localhost",
  user: "root",
  database: "db_resto"
};

// const configCRM = {
//   host: "crm.ama.id",
//   user: "root",
//   password : "4m4@11511",
//   database: "crm_production"
// };

const CONDB = sql.createConnection(config);
// const CON_CRM = sql.createConnection(configCRM);

export default {CONDB}