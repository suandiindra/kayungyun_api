import express from 'express'
import respone from '../utils/respone.js';
import DB from '../config/db.js';
import { v4 as uuidv4 } from 'uuid';

async function tambahtransaksi(req,res){
    const {
        tgl,cabang,platform,dataproduk
    } = req.body;
    let obj = {}
    const unik = uuidv4();

    console.log(tgl,cabang,platform,dataproduk,`xxxxxxxxxxxxx`);

    try {
        const request = await DB.CONDB.promise();
        let qr = `insert into transaksi  (transaksi_id ,period ,m_cabang_id ,m_platform_id ,createddate )
        values ('${unik}' ,'${tgl}' ,'${cabang}' ,'${platform}' ,now() )`

        let res = await request.query(qr)
        if(res){
            let data_trans = JSON.parse(dataproduk)
            if(data_trans.length > 0){
                for(let i =0; i < data_trans.length; i++){
                    let id = data_trans[i].id
                    let sl = `select * from m_produk mp where m_produk_id = '${id}'`
                    let resSL = await request.query(sl)
                    console.log(sl);

                    let nama = resSL[0][0].nama_produk
                    let margin_online = resSL[0][0].margin_online

                    let dtl = `insert into transaksi_detail 
                    (transaksi_detail_id ,transaksi_id ,m_produk_id ,nama_produk ,qty,harga_jual ,margin_online ,createdate )
                    select uuid() ,'${unik}' ,'${data_trans[i].id}' ,'${nama}' ,${data_trans[i].qty},${data_trans[i].harga} ,'${margin_online}' ,now()`
                    await request.query(dtl)
                }
            }   
        }
        obj = respone("200",`Berhasil Insert Data`)
    } catch (error) {
        console.log(error);
        obj = respone("500",error)
        
    }
    return obj
}

export default {tambahtransaksi}
