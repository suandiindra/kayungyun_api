import express from 'express'
import respone from '../utils/respone.js';
import DB from '../config/db.js';

async function getMenuById(req,res){
    try {
        const request = await DB.promise();
        let qr = `select * from m_menu`;
        let data = await request.query(qr)
        return respone("200",data[0])
    } catch (error) {
        return respone("500",error)   
    }
}


export default {getMenuById}