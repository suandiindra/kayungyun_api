import express from 'express'
import respone from '../../utils/respone.js';
import DB from '../../config/db.js';
import { v4 as uuidv4 } from 'uuid';

async function findbom(req,res){
    const {
        query: {m_produk_id }
    } = req;
    let obj = {}
    try {
        const request = await DB.CONDB.promise();
        let filter = ""
        if(m_produk_id){
            filter = ` and mp.m_produk_id = '${m_produk_id}' `
        }
        let qr = `select b.*,c.nama_produk from m_produk mp 
        inner join m_bom b on b.key_bom = mp.m_bom_id 
        inner join m_produk c on c.m_produk_id = b.m_produk_id 
        where 1=1 ${filter}`
        let data = await request.query(qr)
        return respone("200",data[0])
    } catch (error) {
        obj = respone("500",err)
        return obj
    }
}
async function find(req,res){
    const {
        query: {m_produk_id,filter1 }
      } = req;
    let obj = {}
    try {
        const request = await DB.CONDB.promise();
        let filter = ""
        if(filter1){
            filter = filter + ` and ${filter1} `
        }
        if(m_produk_id){
            filter = ` and m_produk_id = '${m_produk_id}' `
        }
        let qr = `select * from m_produk mp where 1=1 ${filter} order by nama_produk asc`
        let data = await request.query(qr)
        return respone("200",data[0])
    } catch (error) {
        obj = respone("500",err)
        return obj
    }
}
async function tambahproduk(req,res){
    const {
       nama_produk,kategori,is_sell,harga_jual,margin_online
       ,m_user_id,m_platform
    } = req.body;
    let obj = {}
    // console.log(req.body);
    try {
        const request = await DB.CONDB.promise();
        let insert = `insert into m_produk
        (m_produk_id ,nama_produk ,kategori ,is_sell ,isactive ,harga_jual ,margin_online ,m_platform,createdby ,created )
        values
        (uuid() ,'${nama_produk}' ,'${kategori}' ,'${is_sell}' ,1 ,${harga_jual ? harga_jual : 0} ,${margin_online ? margin_online : 0} ,'${m_platform}','SYSTEM',now() )`
        
        console.log(insert);
        let data = await request.query(insert)
        if(data){
            obj = respone("200",`Berhasil Insert Data`)
        }else{
            obj = respone("500",`Gagal Insert Data`)
        }
        return obj
    }catch(err){
        console.log(err);
        obj = respone("500",err)
        return obj
    }
}
async function tambahbom(req,res){
    const {
       m_produk_id,databom
    } = req.body;
    let obj = {}
    const unik = uuidv4();
    
    try {
        const request = await DB.CONDB.promise();
        // console.log(m_produk_id,JSON.parse(databom));
        let sx = `select * from m_produk where m_produk_id = '${m_produk_id}' and m_bom_id is not null`;
        let drsx = await request.query(sx)
        let data_bom = JSON.parse(databom)
        let total_biaya_bom = 0;
        if(drsx[0].length > 0){
            let m_bom_id = drsx[0][0].m_bom_id
            console.log(`xzxxxxxxxxxx`,m_bom_id);
            for(let i =0; i < data_bom.length; i++){
                let insert = `insert into m_bom (m_bom_id ,m_produk_id ,qty_bom ,harga ,isactive ,createdate,key_bom )
                values (uuid() ,'${data_bom[i].m_produk_id}' ,${data_bom[i].qty_bom},${data_bom[i].harga} ,1 ,now(),'${m_bom_id}' )`
                await request.query(insert)
                total_biaya_bom = total_biaya_bom + parseFloat(data_bom[i].harga)
            }

            let upd = `update m_produk set harga_beli = harga_beli + ${total_biaya_bom} where m_produk_id = '${m_produk_id}'`;
            let resp = await request.query(upd)
            
            if(resp){
                obj = respone("200",`Berhasil Insert Data`)
            }else{
                obj = respone("500",`Gagal Insert Data`)
            }
        }else{
            
            for(let i =0; i < data_bom.length; i++){
                let insert = `insert into m_bom (m_bom_id ,m_produk_id ,qty_bom ,harga ,isactive ,createdate,key_bom )
                values (uuid() ,'${data_bom[i].m_produk_id}' ,${data_bom[i].qty_bom},${data_bom[i].harga} ,1 ,now(),'${unik}' )`
                await request.query(insert)
                total_biaya_bom = total_biaya_bom + parseFloat(data_bom[i].harga)
            }

            let upd = `update m_produk set m_bom_id = '${unik}',harga_beli = ${total_biaya_bom} where m_produk_id = '${m_produk_id}'`;
            let resp = await request.query(upd)
            
            if(resp){
                obj = respone("200",`Berhasil Insert Data`)
            }else{
                obj = respone("500",`Gagal Insert Data`)
            }  
        }
  
        return obj
    }catch(err){
        console.log(err);
        obj = respone("500",err)
        return obj
    }
}
export default {tambahproduk,tambahbom,find,findbom}