import express from 'express'
import respone from '../utils/respone.js';
import DB from '../config/db.js';
import { v4 as uuidv4 } from 'uuid';


async function findDepartment(req,res){
    try{
        const request = await DB.CONDB.promise();
        let qr = `select * from m_department `;
        let data = await request.query(qr)
        return respone("200",data[0])
    }catch(error){
        obj = respone("500",error)
        return obj
    }
}

async function findMaster(req,res){
    const {
        query: {table,filter1 }
    } = req;
    let obj = {}
    try{
        const request = await DB.CONDB.promise();

        let qr = `select * from ${table} where 1=1 ${filter1 ? filter1 : ""} `;
        console.log(qr);
        let data = await request.query(qr)
        return respone("200",data[0])
    }catch(error){
        console.log(error);
        obj = respone("500",error)
        return obj
    }
}


export default {findDepartment,findMaster}