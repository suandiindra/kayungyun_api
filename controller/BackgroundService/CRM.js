import express from 'express'
// import respone from '../utils/respone.js';
import DB from '../../config/db.js';

async function scheduleA(){
    try {
        let period = new Date();
        let jam = period.getHours()
        const request = await DB.CON_CRM.promise();

        let cek1 = `select vdd.nomor_do ,vdd.customer ,vdd.do_date 
        from v_data_do vdd 
        inner join temp_do_x v on v.nomor_do = vdd.nomor_do 
        where vdd.do_date = DATE_FORMAT(now(),'%Y-%m-%d') 
        order by v.do_date asc`
        let or1 = await request.query(cek1);

        // console.log(or1[0]);
        
        if(or1[0].length == 0){
            let qr = `insert into temp_do_x
            select vdd.nomor_do ,vdd.customer ,vdd.do_date 
            from v_data_do vdd 
            left join temp_do_x v on v.nomor_do = vdd.nomor_do 
            where vdd.do_date = DATE_FORMAT(now(),'%Y-%m-%d') order by v.do_date asc`;
            await request.query(qr)
            
            let pc = `select DISTINCT nomor_do from v_data_do vdd where vdd.do_date = DATE_FORMAT(now(),'%Y-%m-%d')`
            let pcD = await request.query(pc);

            for(let z = 0; z<pcD[0].length; z++){
                // console.log(pcD[0][z].nomor_do);
                let nomordo = pcD[0][z].nomor_do
                let insert = `insert into app_entity_71 (parent_id,parent_item_id ,linked_id,date_added 
                ,date_updated ,created_by ,sort_order ,field_981 
                ,field_982,field_1106,field_2303,field_2398,field_3380) 
                select 0,0 ,0,date_added 
                ,0,0,0 ,'173' 
                ,field_3389,0,'','','' from app_entity_213 where field_3389 = '${nomordo}'`

                let hsl = await request.query(insert)
                if(hsl){
                    console.log(`berhasil ${z}/${pcD[0].length} -- `,nomordo);
                }
            }

        }

        if(jam == "16"){
            let sel = `select * from proses_sync where DATE_FORMAT(create_date,'%Y-%m-%d')  = DATE_FORMAT(now(),'%Y-%m-%d')`
            let or = await request.query(sel);

            console.log(or[0].length);
            if(or[0].length == 0){
                let del = `delete from app_entity_214`
                await request.query(del)

                let masuk = `insert create_date,isproses into proses_sync
                select now(),1`
                await request.query(masuk)
                
            }

            let cek2 = `select * from temp_data_ar_outstanding 
            where DATE_FORMAT(created_date,'%Y-%m-%d') = DATE_FORMAT(now(),'%Y-%m-%d')`
            let da2 = await request.query(cek2);
            if(da2[0].length == 0){
                let cek3 = `select * from app_entity_214
                where DATE_FORMAT(FROM_UNIXTIME(date_added),'%Y-%m-%d') = DATE_FORMAT(now(),'%Y-%m-%d')`
                let da3 = await request.query(cek3);
                if(da3[0].length > 0){
                    let del = `delete from temp_data_ar_outstanding`
                    await request.query(del);

                    let masuktemp = `insert into temp_data_ar_outstanding
                    select *,now() from crm_production.v_outstanding`;
                    await request.query(masuktemp);
                }else{
                    console.log(`belum terupload`);
                }
            }else{
                console.log(`udah oke`);
            }

        }else{
            console.log(`Belum saat nya... schedule-A`);
        }
        
    } catch (error) {
        console.log(`Error : `,error);
        
    }
}

async function scheduleB(){
    const request = await DB.CON_CRM.promise();

    let cek1 = `select vdd.nomor_do from v_data_do vdd 
    inner join do_audit b on b.nomor_do = vdd.nomor_do 
    where vdd.do_date > '2022-02-01'
    and kode1 is null
    and vdd.nomor_do like '%WAN%'`
    let or1 = await request.query(cek1);

    console.log(or1[0]);
    
    if(or1[0].length > 0){
        for(let z = 0; z<or1[0].length; z++){
            let nomordo = or1[0][z].nomor_do
            let insert = `insert into app_entity_71 (parent_id,parent_item_id ,linked_id,date_added 
                ,date_updated ,created_by ,sort_order ,field_981 
                ,field_982,field_1106,field_2303,field_2398,field_3380) 
                select 0,0 ,0,date_added 
                ,0,0,0 ,'173' 
                ,field_3389,0,'','','' from app_entity_213 where field_3389 = '${nomordo}'`
            
            console.log(nomordo);
            let rex = await request.query(insert);
        }
    }
}
export default {scheduleA,scheduleB}