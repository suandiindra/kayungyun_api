import express from 'express'
import TransaksiController  from '../controller/TransaksiController.js';
import DB from '../config/db.js';
import respone from '../utils/respone.js';

const route = express.Router();
route.use(express.urlencoded({ extended: false }));

route.post('/tambahtransaksi',async(req,res)=>{
    console.log("API BOM");
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let response = await TransaksiController.tambahtransaksi(req,res)
        return res.send(response) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(response("500",error))
    }
})
export default route
