import express from 'express'
import UserController  from '../controller/UserController.js';
import DB from '../config/db.js';
import respone from '../utils/respone.js';

const route = express.Router();

route.get('/',async(req,res)=>{
    console.log("API 1");
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let rp = await UserController.find(req,res)
        console.log(rp);
        return res.send(rp) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(response("500",error))
    }
})

route.post('/createUser',async(req,res)=>{
    console.log("API 3");
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let response = await UserController.createUser(req,res)
        return res.send(response) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(response("500",error))
    }
})
route.get('/loginUserzz',async(req,res)=>{
    console.log("API 4");
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        // let response = 
        await UserController.loginUser(req,res)
        // return res.send(response) 
    } catch (error) {
        console.log(error,`eror`);
        return res.status(500).send(error)
    }
})

route.get('/:id',async(req,res)=>{
    console.log("API 2");
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let response = await UserController.findOne(req,res)
        return res.send(response) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(response("500",error))
    }
})
export default route