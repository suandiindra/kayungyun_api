import express from 'express'
import UserController  from '../controller/UserController.js';
import ProdukController  from '../controller/produk/ProdukController.js';
import DB from '../config/db.js';
import respone from '../utils/respone.js';

const route = express.Router();
route.use(express.urlencoded({ extended: false }));


route.post('/tambahproduk',async(req,res)=>{
    console.log("API 3");
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      
        let response = await ProdukController.tambahproduk(req,res)
        return res.send(response) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(response("500",error))
    }
})
route.post('/tambahbom',async(req,res)=>{
    console.log("API BOM");
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let response = await ProdukController.tambahbom(req,res)
        return res.send(response) 
    } catch (error) {
        console.log(error,`eror`);
        return res.send(response("500",error))
    }
})
route.get('/find',async(req,res)=>{
    console.log("API find");
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        // let response = 
        let response = await ProdukController.find(req,res)
        return res.send(response)
    } catch (error) {
        console.log(error,`eror`);
        return res.status(500).send(error)
    }
})
route.get('/findbom',async(req,res)=>{
    console.log("API find");
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        // let response = 
        let response = await ProdukController.findbom(req,res)
        return res.send(response)
    } catch (error) {
        console.log(error,`eror`);
        return res.status(500).send(error)
    }
})
export default route